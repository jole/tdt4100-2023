package oving4;


// copied from øving 1

public class StopWatch {
    int ticks, watchStart, lapStart, lapTime;
    boolean started, stopped;

    StopWatch() {
        this.ticks = 0;
        this.started = false;
        this.stopped = false;
        this.lapTime = -1;
    }

    public boolean isStarted() {
        return this.started;
    }

    public boolean isStopped() {
        return this.stopped;
    }

    public int getTicks() {
        return this.ticks;
    }

    public int getTime() {
        if (!started) {
            return -1;
        }

        return this.ticks - this.watchStart;
    }

    public int getLapTime() {
        if (stopped) {
            return 0;
        }

        if (!started) {
            return -1;
        }

        return this.ticks - this.lapStart;
    }

    public int getLastLapTime() {
        if (stopped) {
            return this.ticks - this.lapStart;
        }

        if (!started) {
            return -1;
        }

        return this.lapTime;
    }

    public void tick(int delta) {
        this.ticks += delta;
        if (this.stopped) {
            // hack to keep incrementing ticks and keep watch/lapStart at same distance to
            // ticks
            this.watchStart += delta;
            this.lapStart += delta;
            return;
        }
    }

    public void start() {
        this.watchStart = this.ticks;
        this.lapStart = this.ticks;
        this.lapTime = -1;
        this.started = true;
        this.stopped = false;
    }

    public void stop() {
        this.stopped = true;
    }

    public void lap() {
        this.lapTime = getLapTime();
        this.lapStart = this.ticks;
    }
    
    public static void main(String[] args) {
        StopWatch w = new StopWatch();
        System.out.println(w);

        w.start();
        System.out.println(w);
        
        w.tick(15);
        w.lap();
        System.out.println(w);

        w.start();
        w.tick(13);
        w.lap();
        System.out.println(w);

        w.stop();
        System.out.println(w);
    }
    
    @Override
    public String toString() {
        String s_started = started ? "Started" : "Not started";
        String s_stopped = stopped ? "Stopped" : "Not stopped";

        return String.format("%11s. %11s. Time: %4d. Last Laptime: %4d.", s_started, s_stopped, getTime(), getLastLapTime());
    }
}
