package oving4;

import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Collectors;

public class StopWatchManager {
    private HashMap<String, StopWatch> watches;

    StopWatchManager() {
        this.watches = new HashMap<>();
    }

    public StopWatch newStopWatch(String name) {
        StopWatch st = new StopWatch();
        this.watches.put(name, st);
        return st;
    }

    public void removeStopWatch(String name) {
        this.watches.remove(name);
    }

    public void tick(int ticks) {
        for (StopWatch sw : this.watches.values()) {
            sw.tick(ticks);
        }
    }

    public StopWatch getStopWatch(String name) {
        return this.watches.get(name);
    }

    public Collection<StopWatch> getAllWatches() {
        return this.watches.values();
    }

    public Collection<StopWatch> getStartedWatches() {
        return this.watches.values().stream().filter(s -> s.isStarted()).collect(Collectors.toList());
    }

    public Collection<StopWatch> getStoppedWatches() {
        return this.watches.values().stream().filter(s -> s.isStopped()).collect(Collectors.toList());
    }
}
