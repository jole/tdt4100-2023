package oving4;

import java.util.ArrayList;
import java.util.Collection;

enum Type {
    Weapon,
    Armour,
    Potion,
    Valuable
}

class Item {
    String name, type;
    double price;
    Merchant owner;

    Item(String name, String type, double price) {
        this.name = name;
        this.type = type;
        this.price = price;
    }

    Merchant getOwner() {
        return this.owner;
    }

    void changeOwner(Merchant owner) {
        this.owner = owner;
    }

    double getPrice() {
        return this.price;
    }

    void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return String.format("%s: %s for %f", this.type, this.name, this.price);
    }
}

public class Merchant {
    private double balance;
    private ArrayList<Item> items;

    Merchant(double balance) {
        if (balance < 0) {
            throw new RuntimeException();
        }
        this.balance = balance;
        this.items = new ArrayList<>();
    }

    void obtainItem(Item i) {
        i.changeOwner(this);
        items.add(i);
    }

    void removeItem(Item i) {
        items.remove(i);
    }

    public double getBalance() {
        return this.balance;
    }

    public Collection<Item> getInventory() {
        return this.items;
    }

    void itemSale(double sale, Item i) {
        if (i.getOwner() != this) {
            throw new RuntimeException();
        }
        if (sale < 0 || sale > 1) {
            throw new RuntimeException();
        }
        i.setPrice(i.getPrice() * (1 - sale));
    }

    void inventorySale(double sale) {
        for (Item item : this.items) {
            itemSale(sale, item);
        }
    }

    void sellItem(Item i, Merchant other) {
        if (i.getOwner() != this | this == other | other.balance < i.getPrice()) {
            throw new IllegalArgumentException();
        }

        this.balance += i.getPrice();
        other.balance -= i.getPrice();

        removeItem(i);
        other.obtainItem(i);
    }

}
