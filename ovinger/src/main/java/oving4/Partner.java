package oving4;

public class Partner {
    private String name;
    private Partner partner;
    
    Partner(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setPartner(Partner other) {
        if(this.partner == other) {
            return; // already partners!
        }

        if(other == null) {
            // we want to end the partnership
            if(this.partner != null) {
                // prevent recursion until stack overflow
                Partner old = this.partner;
                this.partner = null;
                old.setPartner(null);
            }
            this.partner = null;
        } else {
            // we want to start a new partnership
            // first end old ones
            if(this.partner != null) {
                Partner old = this.partner;
                this.partner = null;
                old.setPartner(null);
            }
            
            this.partner = other;
            this.partner.setPartner(this);
        }
    }
    
    public Partner getPartner() {
        return this.partner;
    }
}
