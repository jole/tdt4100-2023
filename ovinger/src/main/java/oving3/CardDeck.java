package oving3;

import java.util.ArrayList;

public class CardDeck {
    private ArrayList<Card> cards;

    private static final char suits[] = { 'S', 'H', 'D', 'C' };

    CardDeck(int n) {
        this.cards = new ArrayList<>();

        if (n < 0 || n > 13) {
            throw new IllegalArgumentException();
        }

        for (int s = 0; s < 4; ++s) {
            for (int i = 1; i <= n; ++i) {
                this.cards.add(new Card(suits[s], i));
            }
        }
    }

    public int getCardCount() {
        return this.cards.size();
    }

    public Card getCard(int n) {
        try {
            return this.cards.get(n);
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    public void shufflePerfectly() {
        ArrayList<Card> left = new ArrayList<>(), right = new ArrayList<>();

        int len = cards.size();
        int mid = len / 2;
        for (int i = 0; i < mid; ++i) {
            left.add(cards.get(i));
        }
        for (int i = mid; i < len; ++i) {
            right.add(cards.get(i));
        }

        cards = new ArrayList<>();
        for (int i = 0; i < mid; ++i) {
            cards.add(left.get(i));
            cards.add(right.get(i));
        }
    }
}
