package oving3;

public class Card {
    private char suit;
    private int nr;

    Card(char suit, int nr) {
        switch (suit) {
            case 'S':
            case 'H':
            case 'D':
            case 'C':
                break;
            default:
                throw new IllegalArgumentException();
        }

        if (nr < 1 || nr > 13) {
            throw new IllegalArgumentException();
        }

        this.suit = suit;
        this.nr = nr;
    }
    
    public char getSuit() {
        return this.suit;
    }
    
    public int getFace() {
        return this.nr;
    }
    
    @Override
    public String toString() {
        return String.format("%c%d", this.suit, this.nr);
    }
}
