package oving5;

import java.util.Iterator;
import java.util.function.BinaryOperator;

public class BinaryComputingIterator implements Iterator<Double> {
    Iterator<Double> it1, it2;
    BinaryOperator<Double> op;

    Double def1, def2;

    public BinaryComputingIterator(Iterator<Double> it1, Iterator<Double> it2, BinaryOperator<Double> op) {
        this.it1 = it1;
        this.it2 = it2;
        this.op = op;
        this.def1 = null;
        this.def2 = null;
    }

    public BinaryComputingIterator(Iterator<Double> it1, Iterator<Double> it2,
            Double default1, Double default2, BinaryOperator<Double> op) {
        this.it1 = it1;
        this.it2 = it2;
        this.op = op;
        this.def1 = default1;
        this.def2 = default2;
    }

    @Override
    public boolean hasNext() {
        boolean both = this.it1.hasNext() && this.it2.hasNext();

        // we need at least one to contain a value
        boolean any = (this.it1.hasNext() && this.def2 != null) || (this.def1 != null && this.it2.hasNext());

        return both || any;
    }

    @Override
    public Double next() {
        Double v1 = (this.it1.hasNext() ? this.it1.next() : this.def1);
        Double v2 = (this.it2.hasNext() ? this.it2.next() : this.def2);

        return this.op.apply(v1, v2);
    }
}
