package oving5;

import java.util.ArrayList;
import java.util.Collections;

public class Card implements Comparable<Card> {

	// the suit (farge), one of the values 'S' (spades), 'H' (hearts), 'D'
	// (diamonds) and 'C' (clubs)
	private char suit;
	// the value, 1 for the ace, 2 - 10, 11 (knight), 12 (queen) and 13 (king). -1
	// is invalid
	private int face = -1;

	// the set of suits in decreasing order
	public final static String SUITS = "SHDC";

	public Card(char suit, int face) {
		if (SUITS.indexOf(suit) < 0) {
			throw new IllegalArgumentException("Illegal suit: " + suit);
		}
		if (face < 1 || face > 13) {
			throw new IllegalArgumentException("Illegal face: " + face);
		}
		this.suit = suit;
		this.face = face;
	}

	/*
	 * Returns suit and face as a string
	 * E.g. Ace of spades is S1 and king of clubs is C13
	 */
	public String toString() {
		return String.valueOf(suit) + face;
	}

	public char getSuit() {
		return this.suit;
	}

	public int getFace() {
		return this.face;
	}

	// task: CardComparison
	public int suitCompareTo(Card o) {
		return SUITS.indexOf(o.getSuit(), 0) - SUITS.indexOf(this.getSuit(), 0);
	}

	@Override
	public int compareTo(Card o) {
		int suitDiff = this.suitCompareTo(o);

		// S -> 0
		// H -> 1
		// S comes after H
		// this: S, o: H -> 1
		// o - this = 1 - 0 = 1

		if (suitDiff != 0) {
			return suitDiff;
		}

		// 1 kommer før 2 -> -1

		return this.getFace() - o.getFace();
	}

	public static void main(String[] args) {
		// D1
		// H4
		// H5
		// H5
		// H6
		// S7
		ArrayList<Card> unsorted = new ArrayList<Card>();
		unsorted.add(new Card('H', 5));
		unsorted.add(new Card('H', 4));
		unsorted.add(new Card('S', 7));
		unsorted.add(new Card('H', 5));
		unsorted.add(new Card('D', 1));
		unsorted.add(new Card('H', 6));

		ArrayList<Card> sorted = new ArrayList<Card>(unsorted);
		Collections.sort(sorted);

		ArrayList<Card> fasit = new ArrayList<Card>();
		fasit.add(new Card('D', 1));
		fasit.add(new Card('H', 4));
		fasit.add(new Card('H', 5));
		fasit.add(new Card('H', 5));
		fasit.add(new Card('H', 6));
		fasit.add(new Card('S', 7));

		assert (sorted.size() == fasit.size());
		for (int i = 0; i < sorted.size(); ++i) {
			Card a = fasit.get(i);
			Card b = sorted.get(i);
			assert (a.compareTo(b) == 0);
		}

		System.out.println("Great success!");
	}
}
