package oving5;

import java.util.Comparator;

public class CardComparator implements Comparator<Card> {
    boolean aceHigh;
    char trumpSuit;

    public CardComparator(boolean aceHigh, char trumpSuit) {
        this.aceHigh = aceHigh;
        this.trumpSuit = trumpSuit;
    }
    
    public int transformFace(int i) {
        if(!aceHigh) {
            return i;
        }
        
        if(i == 1) {
            return 14;
        } else {
            return i;
        }
    }

    @Override
    public int compare(Card o1, Card o2) {
        boolean t1 = o1.getSuit() == this.trumpSuit;
        boolean t2 = o2.getSuit() == this.trumpSuit;
        
        // check if one card trumps but the other doesnt
        int trumpDiff = (t1?1:0) - (t2?1:0);
        if(trumpDiff != 0) {
            return trumpDiff;
        }
        
        // otherwise check normal suitDiff
		int suitDiff = o1.suitCompareTo(o2);
		if(suitDiff != 0) {
			return suitDiff;
		}
        
        // lastly, compare numbers
        int f1 = transformFace(o1.getFace());
        int f2 = transformFace(o2.getFace());
		return f1 - f2;
	}
}
