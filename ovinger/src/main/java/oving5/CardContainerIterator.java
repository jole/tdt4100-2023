package oving5;

import java.util.Iterator;

public class CardContainerIterator implements Iterator<Card> {
    CardContainer container;
    int i;

    CardContainerIterator(CardContainer container) {
        this.container = container;
        this.i = 0;
    }

    @Override
    public boolean hasNext() {
        return this.i < container.getCardCount();
    }

    @Override
    public Card next() {
        if(!hasNext()) {
            throw new IllegalAccessError();
        }
        
        Card card = container.getCard(this.i);
        this.i++;
        return card;
    }
}
