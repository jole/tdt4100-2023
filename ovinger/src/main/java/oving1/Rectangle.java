package oving1;

public class Rectangle {
    int x1, y1, x2, y2;

    Rectangle(int x1, int y1, int x2, int y2) {
        this.x1 = Math.min(x1, x2);
        this.y1 = Math.min(y1, y2);
        this.x2 = Math.max(x1, x2);
        this.y2 = Math.max(y1, y2);
    }

    public int getMinX() {
        return this.x1;
        // return Math.min(this.x1, this.x2);
    }

    public int getMaxX() {
        return this.x2;
        // return Math.max(this.x1, this.x2);
    }

    public int getMinY() {
        return this.y1;
        // return Math.min(this.y1, this.y2);
    }

    public int getMaxY() {
        return this.y2;
        // return Math.max(this.y1, this.y2);
    }

    public int getWidth() {
        return this.getMaxX() - this.getMinX();
    }

    public int getHeight() {
        return this.getMaxY() - this.getMinY();
    }

    public boolean isEmpty() {
        return this.getWidth() == 0 || this.getHeight() == 0;
    }

    public boolean contains(int x, int y) {
        if (isEmpty()) {
            return false;
        }
        boolean w = x >= this.getMinX() && x <= this.getMaxX();
        boolean h = y >= this.getMinY() && y <= this.getMaxY();
        return w && h;
    }

    public boolean contains(Rectangle other) {
        if (isEmpty()) {
            return false;
        }
        boolean tl = this.contains(other.getMinX(), other.getMinY());
        boolean tr = this.contains(other.getMinX(), other.getMaxY());
        boolean bl = this.contains(other.getMaxX(), other.getMinY());
        boolean br = this.contains(other.getMaxX(), other.getMaxY());
        return tl && tr && bl && br;
    }

    public boolean add(int x, int y) {
        if (contains(x, y)) {
            return false;
        }

        this.x1 = Math.min(this.x1, x);
        this.x2 = Math.max(this.x2, x);

        this.y1 = Math.min(this.y1, y);
        this.y2 = Math.max(this.y2, y);

        return true;
    }

    public boolean add(Rectangle rect) {
        // `|` because `||` short-circuits
        return add(rect.getMinX(), rect.getMinY()) |
                add(rect.getMinX(), rect.getMaxY()) |
                add(rect.getMaxX(), rect.getMaxY()) |
                add(rect.getMaxX(), rect.getMinY());
    }

    public Rectangle union(Rectangle rect) {
        Rectangle n = new Rectangle(rect.x1, rect.y1, rect.x2, rect.y2);
        n.add(this);
        return n;
    }
    
    public static void main(String[] args) {
        Rectangle r = new Rectangle(2, 2, 7, 8);
        System.out.println(r);
    }
    
    @Override
    public String toString() {
        return String.format("(%d, %d) to (%d, %d)", getMinX(), getMinY(), getMaxX(), getMaxY());
    }
}
