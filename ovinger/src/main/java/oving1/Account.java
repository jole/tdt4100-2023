package oving1;

public class Account {
    // this is stupid, we should just use fixed point integers (i.e. count cents/øre)
    private double balance;
    private double interestRate;

    public void deposit(double d) {
        if(d <= 0) {
            return;
        }
        
        this.balance += d;
    }
    
    public void addInterest() {
        this.balance += this.balance * interestRate / 100;
    }
    
    public double getBalance() {
        return this.balance;
    }
    
    public double getInterestRate() {
        return this.interestRate;
    }
    
    public void setInterestRate(double rate) {
        this.interestRate = rate;
    }
    
    public static void main(String[] args) {
        Account ac = new Account();
        assert(ac.balance == 0.0);
        assert(ac.interestRate == 0.0);

        ac.setInterestRate(1.5);
        assert(ac.balance == 0.0);
        assert(ac.interestRate == 1.5);

        ac.deposit(1000);
        assert(ac.balance == 1000.0);
        assert(ac.interestRate == 1.5);

        ac.addInterest();
        assert(ac.balance == 1015.0);
        assert(ac.interestRate == 1.5);
        
        System.out.println("Success!");
        
        System.out.println(ac);
    }
    
    @Override
    public String toString() {
        return String.format("Balance: %.1f, Interest: %.1f", balance, interestRate);
    }
}
