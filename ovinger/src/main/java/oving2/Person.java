package oving2;

import java.util.Date;

public class Person {
    private String name;
    private Date birthday;
    private char gender;
    private String email;

    private static final String[] countries = {
            "ad", "ae", "af", "ag", "ai", "al", "am", "ao", "aq", "ar", "as", "at", "au", "aw", "ax", "az", "ba", "bb",
            "bd", "be", "bf", "bg", "bh", "bi", "bj", "bl", "bm", "bn", "bo", "bq", "br", "bs", "bt", "bv", "bw", "by",
            "bz", "ca", "cc", "cd", "cf", "cg", "ch", "ci", "ck", "cl", "cm", "cn", "co", "cr", "cu", "cv", "cw", "cx",
            "cy", "cz", "de", "dj", "dk", "dm", "do", "dz", "ec", "ee", "eg", "eh", "er", "es", "et", "fi", "fj", "fk",
            "fm", "fo", "fr", "ga", "gb", "gd", "ge", "gf", "gg", "gh", "gi", "gl", "gm", "gn", "gp", "gq", "gr", "gs",
            "gt", "gu", "gw", "gy", "hk", "hm", "hn", "hr", "ht", "hu", "id", "ie", "il", "im", "in", "io", "iq", "ir",
            "is", "it", "je", "jm", "jo", "jp", "ke", "kg", "kh", "ki", "km", "kn", "kp", "kr", "kw", "ky", "kz", "la",
            "lb", "lc", "li", "lk", "lr", "ls", "lt", "lu", "lv", "ly", "ma", "mc", "md", "me", "mf", "mg", "mh", "mk",
            "ml", "mm", "mn", "mo", "mp", "mq", "mr", "ms", "mt", "mu", "mv", "mw", "mx", "my", "mz", "na", "nc", "ne",
            "nf", "ng", "ni", "nl", "no", "np", "nr", "nu", "nz", "om", "pa", "pe", "pf", "pg", "ph", "pk", "pl", "pm",
            "pn", "pr", "ps", "pt", "pw", "py", "qa", "re", "ro", "rs", "ru", "rw", "sa", "sb", "sc", "sd", "se", "sg",
            "sh", "si", "sj", "sk", "sl", "sm", "sn", "so", "sr", "ss", "st", "sv", "sx", "sy", "sz", "tc", "td", "tf",
            "tg", "th", "tj", "tk", "tl", "tm", "tn", "to", "tr", "tt", "tv", "tw", "tz", "ua", "ug", "um", "us", "uy",
            "uz", "va", "vc", "ve", "vg", "vi", "vn", "vu", "wf", "ws", "ye", "yt", "za", "zm", "zw"
    };

    Person() {

    }

    public void setName(String name) throws Exception {
        String[] names = name.split(" ", 3);

        if (names.length > 2) {
            throw new IllegalArgumentException("Too many names!");
        }
        if (names.length < 2) {
            throw new IllegalArgumentException("Too few names!");
        }

        if (names[0].length() < 2 || names[1].length() < 2) {
            throw new IllegalArgumentException("First- or last name too short!");
        }

        for (int i = 0; i < name.length(); i++) {
            boolean letter = Character.isLetter(name.charAt(i));
            boolean space = name.charAt(i) == ' ';
            if (!letter && !space) {
                throw new IllegalArgumentException("Invalid character in name");
            }
        }

        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setEmail(String email) {
        String[] parts = email.split("@", 3);
        if (parts.length != 2) {
            throw new IllegalArgumentException();
        }

        String[] names = parts[0].split("\\.", 3);
        if (names.length != 2) {
            throw new IllegalArgumentException();
        }
        if (names[0].length() < 1 || names[1].length() < 1) {
            throw new IllegalArgumentException();
        }

        String[] location = parts[1].split("\\.", 3);
        if (location.length != 2) {
            throw new IllegalArgumentException();
        }
        if (location[0].length() < 1) {
            throw new IllegalArgumentException();
        }
        if (location[1].length() != 2) {
            throw new IllegalArgumentException("tld isnt exactly 2 chars");
        }

        boolean country = false;
        for(int i = 0; i < countries.length; ++i) {
            if(location[1].equals(countries[i])) {
                country = true;
                break; // ee
            }
        }
        
        if(!country) {
            throw new IllegalArgumentException("Bad tld");
        }

        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }

    public void setBirthday(Date date) {
        Date now = new Date();
        if (date.after(now)) {
            throw new IllegalArgumentException("Bad date");
        }
        this.birthday = date;
    }

    public Date getBirthday() {
        return this.birthday;
    }

    public void setGender(char gender) {
        if (gender != 'M' && gender != 'F' && gender != '\0') {
            throw new IllegalArgumentException("Bad gender");
        }
        this.gender = gender;
    }

    public char getGender() {
        return this.gender;
    }
}
