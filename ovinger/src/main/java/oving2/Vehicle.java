package oving2;

public class Vehicle {
    private String reg;
    private char type;
    private char drivstoff;

    private void validate_char(char c) throws IllegalArgumentException {
        if (c < 'A' || c > 'Z') {
            throw new IllegalArgumentException();
        }
        return;
    }

    private void validate(char type, char drivstoff, String reg) throws IllegalArgumentException {
        switch (type) {
            case 'M':
                if (reg.length() == 6) {
                    break;
                }
                throw new IllegalArgumentException();
            case 'C':
                if (reg.length() == 7) {
                    break;
                }
                throw new IllegalArgumentException();
            default:
                throw new IllegalArgumentException();
        }

        String letters = reg.substring(0, 2);
        String numbers = reg.substring(2, reg.length());

        boolean hydrogen = letters.equals("HY");
        boolean electric = letters.equals("EL") || letters.equals("EK");
        boolean fossil = !hydrogen && !electric;

        validate_char(letters.charAt(0));
        validate_char(letters.charAt(1));

        try {
            Integer.parseInt(numbers);
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }

        switch (drivstoff) {
            case 'H':
                if (hydrogen && type == 'C') {
                    break;
                }
                throw new IllegalArgumentException();
            case 'E':
                if (electric) {
                    break;
                }
                throw new IllegalArgumentException();
            case 'D':
            case 'G':
                if (fossil) {
                    break;
                }
                throw new IllegalArgumentException();
            default:
                throw new IllegalArgumentException();
        }
    }

    Vehicle(char type, char drivstoff, String reg) {
        validate(type, drivstoff, reg);

        this.type = type;
        this.drivstoff = drivstoff;
        this.reg = reg;
    }

    public void setRegistrationNumber(String reg) {
        validate(this.type, this.drivstoff, reg);

        this.reg = reg;
    }

    public String getRegistrationNumber() {
        return this.reg;
    }

    public char getVehicleType() {
        return this.type;
    }

    public char getFuelType() {
        return this.drivstoff;
    }
}
